from transformers import EncoderDecoderModel, AutoTokenizer
import torch


class Predictor:
    def __init__(self, model_name_or_path):
        self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
        self.model = EncoderDecoderModel.from_pretrained(model_name_or_path)
        self.model.to(self.device)
        self.tokenizer = AutoTokenizer.from_pretrained(model_name_or_path)

    def __call__(self, text):
        input_ids = self.tokenizer(
            text, truncation=True, max_length=256, return_tensors="pt"
        )["input_ids"].to(self.device)

        out = self.model.generate(
            input_ids,
            num_return_sequences=10,
            top_p=0.90,
            top_k=10,
            no_repeat_ngram_size=1,
        )

        return self.tokenizer.batch_decode(out, skip_special_tokens=True)
