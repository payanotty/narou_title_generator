import requests
import gzip
import pandas as pd
import datetime
import tqdm
import time
import os

if __name__ == "__main__":
    api_url = "https://api.syosetu.com/novelapi/api/"
    df = pd.DataFrame()
    endtime = int(datetime.datetime.now().timestamp())

    cnt = len(df)
    while cnt < 20000:
        time_range = str(endtime - 3600) + "-" + str(endtime)
        payload = {
            "out": "json",
            "gzip": 5,
            "order": "new",
            "lim": 500,
            "lastup": time_range,
            "of": "t-s-ua",  # 出力[t: タイトル, s: あらすじ, ua: 更新日時]
        }
        res = requests.get(api_url, params=payload).content
        r = gzip.decompress(res).decode("utf-8")

        df_temp = pd.read_json(r).drop(0)
        df = pd.concat([df, df_temp])

        # time_rangeを1時間ずつずらす
        endtime -= 3600
        cnt = len(df["title"].unique())
        print(cnt)
        time.sleep(2)

    if not os.path.exists("../data"):
        os.makedirs("../data")
    df.to_csv("../data/narou_titles.csv")
