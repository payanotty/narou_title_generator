import pickle
import pandas as pd
from pandas import DataFrame
from typing import List, Dict
import datasets
from transformers import AutoTokenizer
from transformers import EncoderDecoderModel
from transformers import Seq2SeqTrainingArguments, Seq2SeqTrainer
from tqdm import tqdm

tqdm.pandas()
import matplotlib.pyplot as plt
import torch
from torch import Tensor
import random
import hydra
import os


def create_dataset(
    df: DataFrame, tokenizer, title_max_length: int, story_max_length: int
) -> Dict[str, Tensor]:
    encodings = []

    for row in tqdm(df.itertuples(), total=df.shape[0]):
        inputs = tokenizer.encode_plus(
            row.story,
            padding="max_length",
            truncation=True,
            max_length=story_max_length,
        )

        # デコーダー側のインプットにはタイトルを入れる
        outputs = tokenizer.encode_plus(
            row.title,
            padding="max_length",
            truncation=True,
            max_length=title_max_length,
        )

        inputs["decoder_input_ids"] = outputs["input_ids"]
        inputs["decoder_attention_mask"] = outputs["attention_mask"]

        # ラベル(生成文)はタイトル
        labels = outputs["input_ids"]
        inputs["labels"] = [
            -100 if token == tokenizer.pad_token_id else token for token in labels
        ]

        inputs = {k: torch.tensor(v) for k, v in inputs.items()}
        encodings.append(inputs)
    return encodings


def split_dataset(dataset, train_ratio):
    random.shuffle(dataset)
    train_size = int(train_ratio * len(dataset))
    train_data, val_data = dataset[:train_size], dataset[train_size:]
    return train_data, val_data


@hydra.main(config_path="../config", config_name="config")
def main(cfg):
    print(f"Current working directory: {os.getcwd()}")
    print(f"Original working directory : {hydra.utils.get_original_cwd()}")
    os.chdir(hydra.utils.get_original_cwd())

    TITLE_MAX_LENGTH = 85
    STORY_MAX_LENGTH = 256
    TRAIN_RATIO = 0.99

    df = pd.read_csv("../data/narou_titles.csv")

    tokenizer = AutoTokenizer.from_pretrained(cfg.pretrained)

    dataset = create_dataset(df, tokenizer, TITLE_MAX_LENGTH, STORY_MAX_LENGTH)
    train_data, val_data = split_dataset(dataset, TRAIN_RATIO)

    japanese_bert2bert = EncoderDecoderModel.from_encoder_decoder_pretrained(
        cfg.pretrained, cfg.pretrained
    )

    # set special tokens
    japanese_bert2bert.config.decoder_start_token_id = tokenizer.cls_token_id
    japanese_bert2bert.config.eos_token_id = tokenizer.sep_token_id
    japanese_bert2bert.config.pad_token_id = tokenizer.pad_token_id

    # sensible parameters for beam search
    japanese_bert2bert.config.vocab_size = japanese_bert2bert.config.decoder.vocab_size
    japanese_bert2bert.config.max_length = cfg.bert2bert_params.max_length
    japanese_bert2bert.config.min_length = cfg.bert2bert_params.min_length
    japanese_bert2bert.config.no_repeat_ngram_size = (
        cfg.bert2bert_params.no_repeat_ngram_size
    )
    japanese_bert2bert.config.early_stopping = cfg.bert2bert_params.early_stopping
    japanese_bert2bert.config.length_penalty = cfg.bert2bert_params.length_penalty
    japanese_bert2bert.config.num_beams = cfg.bert2bert_params.num_beams

    rouge = datasets.load_metric("rouge")

    def compute_metrics(pred):
        labels_ids = pred.label_ids
        pred_ids = pred.predictions

        pred_str = tokenizer.batch_decode(pred_ids, skip_special_tokens=True)
        labels_ids[labels_ids == -100] = tokenizer.pad_token_id
        label_str = tokenizer.batch_decode(labels_ids, skip_special_tokens=True)

        rouge_output = rouge.compute(
            predictions=pred_str, references=label_str, rouge_types=["rouge2"]
        )["rouge2"].mid

        return {
            "rouge2_precision": round(rouge_output.precision, 4),
            "rouge2_recall": round(rouge_output.recall, 4),
            "rouge2_fmeasure": round(rouge_output.fmeasure, 4),
        }

    training_args = Seq2SeqTrainingArguments(
        output_dir="../model",
        evaluation_strategy="steps",
        per_device_train_batch_size=cfg.training_args.train_batch_size,
        per_device_eval_batch_size=cfg.training_args.eval_batch_size,
        gradient_accumulation_steps=cfg.training_args.gradient_accumulation_steps,
        predict_with_generate=True,
        logging_steps=10,
        save_steps=600,
        eval_steps=150,
        warmup_ratio=cfg.training_args.warmup_ratio,
        overwrite_output_dir=True,
        save_total_limit=3,
        fp16=True,
        num_train_epochs=cfg.training_args.epochs,
    )

    # instantiate trainer
    trainer = Seq2SeqTrainer(
        model=japanese_bert2bert,
        tokenizer=tokenizer,
        args=training_args,
        compute_metrics=compute_metrics,
        train_dataset=train_data,
        eval_dataset=val_data,
    )

    trainer.train()


if __name__ == "__main__":
    main()
